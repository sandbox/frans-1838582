<?php



function mytest_menu() {
  $items['test'] = array(
    'title' => 'Example',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mytest_form'),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}


function mytest_form($form, &$form_state) {
  $form['lalala'] = array(
    '#type' => 'textfield',
    '#default_value' => 'text',
    '#required' => TRUE,
    '#title' => 'woop'
  );
  $form['test_state'] = array(
    '#type' => 'hms_matrix',
    '#title' => 'woo1p',
    '#row_header' => array(
      array(
        '#type' => 'title',
        '#label' => 'tralala',
        '#key' => 3,
      ),
      array(
        '#type' => 'value',
        '#label' => 'tralala2',
        '#key' => 4,
      ),
      array(
        '#type' => 'value',
        '#label' => 'tralala3',
        '#key' => 5,
      ),
      array(
        '#type' => 'calc',
        '#label' => 'total',
        '#key' => 'sum',
        '#calc_type' => 'colsum',
        //'#calc_value_rows' => array(3, 4, 5),
      ),
    ),
    '#col_header' => array(
      array(
        '#type' => 'title',
        '#label' => 'maandag',
        '#key' => 1,
      ),
      array(
        '#type' => 'title',
        '#label' => 'dinsdag',
        '#key' => 2,
      ),
      array(
        '#type' => 'title',
        '#label' => 'woensdag',
        '#key' => 3,
      ),
      array(
        '#type' => 'calc',
        '#label' => 'total',
        '#key' => 'sum',
        '#calc_type' => 'rowsum',
        //'#calc_value_cols' => array(3, 1),
      ),
    ),
    '#default_value' => array(4 => array(2 => 100)),
    '#input' => FALSE,
  );
  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Submit button text'),
    '#description' => t('Text to display in the submit button of the exposed form.'),
  );
  return $form;
}

function mytest_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  dpm ($form_state);
}